package S2;

import javax.swing.*;
import java.util.Random;


public class S2 {
    public JButton button1;
    public JTextArea textArea1;
    public JPanel window;


    public static void main(String[] args) {
        JFrame frame = new JFrame("S2");
        frame.setContentPane(new S2().window);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        var s2 = new S2();
        Random rand = new Random();
        String newline = "\n";



        s2.button1.addActionListener(e -> {

            int nr = rand.nextInt();

            s2.textArea1.append(nr + newline);
            frame.setContentPane(new S2().window);

        });


    }
}
